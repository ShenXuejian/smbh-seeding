import utilities as ut
import numpy as np 

import matplotlib.pyplot as plt

#collapsed fraction
#fcoll = 2e-4
def fcoll(logM): 
	return 1e-4

#duty cycle
def fduty(logMbh):
	return 1

def lamb_vs_Lbol(Lbol):
	median = 0.469 * Lbol - 22.46
	#median = -1.5 
	sigma = 0.4
	return median, sigma

def Lbol_to_mass(Lbol):
	C = np.log10(1.26e38)
	median, sigma = lamb_vs_Lbol(Lbol)
	lamb = np.random.normal(median, sigma, size=nsample)
	return Lbol - C -lamb

mock_volume = 100. #Mpc^3
nsample = 30000
def generate_ensemble(Lbol, LogPhi):
	Masses = np.array([])
	Weights = np.array([])
	binlength = Lbol[5]-Lbol[4]
	for i in range(len(Lbol)):
		Masses = np.append(Masses, Lbol_to_mass(Lbol[i]))
		Weights = np.append(Weights, np.ones(nsample)*10**LogPhi[i]*mock_volume*binlength/nsample)
	return Masses, Weights
	
def get_mass_function(Masses, Weights):
	bins = np.linspace(5., 12., 26)
	lenbin = bins[5]-bins[4]
	result,_ = np.histogram(Masses, weights=Weights, bins=bins)
	result = result/lenbin/mock_volume
	result = np.log10(result)
	return (bins[1:]+bins[:-1])/2., result




#bol QLF at z=6
Lbol0, Phi0 = ut.return_bolometric_qlf(6, model='B')
Lmin = 45
Lmax = 48.5
plt.plot(Lbol0, Phi0, c='royalblue', lw=2, label=r"$\rm Shen+$ $\rm 2020$")

h=0.7
hmf = np.genfromtxt("./hmf/mVector_1 .txt")
Mhalo   = np.log10(hmf[:,0]/h)
Phihalo = np.log10(hmf[:,7] * h**3) 

Mbh = Mhalo + np.log10(fcoll(Mhalo))

Lbol = Mbh + np.log10(1.26e38)
Phi = Phihalo + np.log10(fduty(Mbh))

plt.plot(Lbol, Phi, c='crimson', lw=2, label=r"$\rm model$")
plt.plot(Lbol + np.log10(0.1), Phi, ':', c='crimson', label=r"$\lambda_{\rm edd} = 0.1$")

plt.axvspan(Lmin, Lmax, color='gray', alpha=0.3)

plt.legend()

plt.ylim(-11, -1.5)
plt.xlim(Lmin-1.5, Lmax+1)
plt.xlabel(r"$L_{\rm bol}\,[{\rm erg}/{\rm s}]$")
plt.ylabel(r"$\log{(\Phi\,[{\rm cMpc}^{-3}\,{\rm dex}^{-1}])}$")

plt.show()