import numpy as np
import scipy.interpolate as interp
from astropy.cosmology import FlatLambdaCDM
hubble = 0.7
Omega0 = 0.3
cosmo = FlatLambdaCDM(H0=hubble*100, Om0=Omega0)

def halo_mass_function(redshift):
    h=0.7
    #hmf = np.genfromtxt("./halo_mass_function/mVector_z="+str(int(redshift))+".dat")
    #logM   = np.log10(hmf[:,0] /h)       # Msun
    #logPhi = np.log10(hmf[:,7] * h**3)  # /cMpc^3/dex
    hmf = np.genfromtxt("../halo_mass_function/hmf_at_z="+str(int(redshift))+".txt")
    logM   = np.log10(hmf[0,:] /h)       # Msun
    logPhi = np.log10(hmf[1,:] * h**3)  # /cMpc^3/dex
    return logM, logPhi
    
def stat_weight(halomass, redshift):
    N_per_logM = 4.
    logM, logPhi = halo_mass_function(redshift)
    hmf = interp.interp1d(logM, logPhi)
    if (halomass<= np.min(logM)) or (halomass>= np.max(logM)):
        return 0
    else:
        return 10**hmf(halomass) / N_per_logM # in unit of per unit comoving volume

################################################################################
def eddington_limit_growth(deltat):
    # eddington limit growth
    # in unit of Gyr
    return np.exp(deltat*1e3/50.) #Salpeter time scale 50Myr assuming epsilon=0.1
    
def vcirc_flat(Mhalo):
    # asymptotic flat circular velocity [km/s] of a halo of mass Mhalo[Msun]
    # Ferrarese 2002
    return np.power( Mhalo/1e12/1.4, 1/3.32 ) * 200
    
def bulge_velocity_dispersion(Mhalo):
    # Ferrarese 2002; Volonteri+ 2003
    log_sigma = (np.log10(vcirc_flat(Mhalo)) - 0.47)/0.88
    return 10**log_sigma

def mass_barrier(concentration, redshift, sigma):
    epsilon = 0.1 # define what is <<
    return (1./epsilon)**3 * 1.02e11 / (concentration/4.)**(21./2) / (1.+ redshift)**6 / (sigma/1.)**3

################################################################################

class bh_tree:
    def __init__(self, tree):
        self.redshift = tree["redshift"].copy()      # a redshift grid
        self.bhmass = np.zeros(tree["mass"].shape)   # mass array of SMBHs, null 0
        self.hostmass = tree["mass"].copy()          # mass array of "all" the haloes that appear in the tree at different z, null -99
        self.parentid = tree["ParentID"].copy()      # parent id of haloes,  null -99; root -1
        self.concentration = tree["concentration"]   # concentration of haloes
        self.time = tree["CosmicTime"].copy()        # a cosmic time grid
        self.order = tree["order"].copy()            # order of the haloes on the tree, null -99
        self.quasar = None
        self.angular_momentum = None
        self.host = tree
        
        self.concentration[np.isfinite(self.hostmass) & (self.concentration<0)] = 4.
    
    def assign_angular_momentum(self):
        # assign angular momentum to the haloes in the tree
        self.angular_momentum = np.random.lognormal(mean=0.05, sigma=0.5, size= self.hostmass.shape)

    def seeding(self, idz, verbose=False): 
        #seed the SMBH at snapshot idz
        f_coll = 2e-4 # collapsed fraction
        
        sigma = 0.1 # cross section, [cm^2/g]
        
        mass_cut = mass_barrier(self.concentration[:,idz], self.redshift[idz], sigma) # halo mass cut for BH seeding, [Msun]
        if verbose: 
            print("mass cut:", mass_cut[:20])
        
        # seed the haloes that are valid (exist mass > 0 & mass > masscut) and the current bh mass is smaller than the seed mass
        id_to_seed =(self.hostmass[:,idz]>0) & (self.hostmass[:,idz]>mass_cut) & (self.bhmass[:,idz]<f_coll*self.hostmass[:,idz])
        
        # our assumption here is that, as long as the criteria hold, whatever processes
        # (collapse, dark accretion, merger) would keep the ratio between bh mass and halo mass invariant
        self.bhmass[id_to_seed,idz] = f_coll * self.hostmass[id_to_seed, idz]
    
    def dmbh_mjmerger(self, mhalo_final): 
        # calculate the change in bh mass of a merged halo and the resulting quasar radiation
        H = 1. # a free parameter of order unity
        delta_mbh = 9e7 * H * (vcirc_flat(mhalo_final)/200.)**4.24  # Natarajan 2014
        #self.quasar["z"] = np.append(self.quasar["z"],self.redshift[idz]) #the redshift of the onset of the event
        #self.quasar["macc"] = np.append(self.quasar["macc"],mbh_final - mbh_dry) #total accreted mass during the event
        #self.quasar["mbh"] = np.append(self.quasar["mbh"],mbh_final) #final bh mass
        return delta_mbh
    
    def mbh_merger(self, mhalo1, mhalo2, mbh1, mbh2, threshold = 0.3):
        # calculate the final mass of two merged bhs
        mtot1 = mhalo1 + mbh1
        mtot2 = mhalo2 + mbh2
        # only when the mass ratio larger than a threshold will the dynamic friction drags the minor bh to the center
        if min(mtot2/mtot1,mtot1/mtot2)>threshold:
            return mbh1+mbh2
        else:
            return max(mbh1,mbh2)
        
    def evolve(self, idz):
        #evolve the bhs at idz to idz-1
        #print("total halo #", np.sum(self.hostmass[:,idz-1]>0))
        
        for i in range(len(self.bhmass[:,idz-1])):
            if (self.hostmass[i,idz-1]>0): #it has to be a valid halo
                
                merged_sons = (self.parentid[:,idz]==i) & (self.hostmass[:,idz]>0) & (self.hostmass[:,idz-1]<0)
                Nmerge = len(self.hostmass[merged_sons,idz])
                
                if Nmerge == 1: #one halo merging onto the parent
                    print("processing halo ", i, " with ", Nmerge, " merging sons")
                    
                    mhalo1, mhalo2 = self.hostmass[i,idz], self.hostmass[merged_sons,idz]
                    mbh1  , mbh2   = self.bhmass[i,idz]      , self.bhmass[merged_sons,idz]
                    mbh_dry = self.mbh_merger(mhalo1, mhalo2, mbh1, mbh2)
                    if mbh_dry!=0:
                        if min(mhalo1/mhalo2,mhalo2/mhalo1)>0.1:  # check if it is a major merger
                            mbh_final = mbh_dry + self.dmbh_mjmerger(self.hostmass[i,idz-1])
                        else:
                            mbh_final = mbh_dry
                    else: # no blackholes at all in both galaxies
                        mbh_final = 0
                elif Nmerge > 1: # more than one halo merging onto the parent
                    idsort = np.argsort(self.hostmass[merged_sons,idz])
                    
                    # only consider the major progenitor
                    mhalo1, mhalo2 = self.hostmass[i,idz], self.hostmass[merged_sons,idz][idsort[-1]]
                    mbh1  , mbh2   = self.bhmass[i,idz]      , self.bhmass[merged_sons,idz][idsort[-1]]
                    mbh_dry = self.mbh_merger(mhalo1, mhalo2, mbh1, mbh2)
                    if mbh_dry!=0:
                        if min(mhalo1/mhalo2,mhalo2/mhalo1)>0.1: 
                            mbh_final = mbh_dry + self.dmbh_mjmerger(self.hostmass[i,idz-1])
                        else:
                            mbh_final = mbh_dry
                    else:
                        mbh_final = 0
                        
                elif Nmerge == 0: # no merger, simply inherit the original bh mass
                    mbh_final = self.bhmass[i,idz]
                
                # the resulting bh cannot be lighter than the original one
                if mbh_final < self.bhmass[i,idz]: mbh_final = self.bhmass[i,idz]
                
                self.bhmass[i,idz-1] = mbh_final 
        
    def simulate(self, zinit, zfinal):
        zids = np.arange(0, len(self.redshift), dtype=np.int32)
        id_initial = zids[self.redshift<zinit][-1]
        id_final = zids[self.redshift>zfinal][0]
        print(id_final, id_initial)
        self.seeding(id_initial, verbose=True)
        print("seeding done")
        i=id_initial
        while i>id_final:
            print("doing snapshot:", i)
            self.seeding(i) #seed the new haloes in snap i that havent been seeded
            self.evolve(i) #evolve i to i-1
            i=i-1