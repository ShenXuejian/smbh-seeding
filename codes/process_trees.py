import numpy as np 
from os import listdir
from os.path import isfile, join
import h5py

from utility import *

def process_one_tree(fname):
    tree = np.load(fname)
    bhs = bh_tree(tree)
    zstart = 20. # no more than 20.10 due to limitation of the tree
    zfinal = 4.
    bhs.simulate(zstart, zfinal)
    return bhs

#loop over all the merger trees
datapath='../treedata/'

tree_names  = np.array([f for f in listdir(datapath) if (isfile(join(datapath, f)) and f.startswith('tree'))])

bh_catalog_all = {
    'mass':     None,
    'hostmass': None,
    'weight':   None
}

redshift = 4. # the end of the tree is at z=4

for i, fname in enumerate(tree_names):
    if i>1: continue
    print("proceesing tree ", fname)
    
    tree_mass = float(fname.strip('.npz').strip('tree_lgM').strip())
    
    bh_catalog_single = process_one_tree(datapath+fname)
    redshift_list = bh_catalog_single.redshift

    weight_single = np.ones(bh_catalog_single.bhmass.shape, dtype=np.float64)*stat_weight(tree_mass, redshift=4)
    
    if i==0:
        bh_catalog_all['mass']     = np.zeros((1,len(redshift_list)))
        bh_catalog_all['hostmass'] = np.zeros((1,len(redshift_list)))
        bh_catalog_all['weight']   = np.zeros((1,len(redshift_list)))
    bh_catalog_all['mass'] = np.concatenate((bh_catalog_all['mass'], bh_catalog_single.bhmass), axis=0)
    bh_catalog_all['hostmass'] = np.concatenate((bh_catalog_all['hostmass'], bh_catalog_single.hostmass), axis=0)
    bh_catalog_all['weight'] = np.concatenate((bh_catalog_all['weight'], weight_single), axis=0)

#save the bh_catalog to hdf5 file
fname="./output/smbh_catalog.hdf5"
with h5py.File(fname,mode='w') as f:
        f.create_dataset("redshift", data=redshift_list)

        f.create_dataset("mass", data=bh_catalog_all['mass'])
        f.create_dataset("hostmass", data=bh_catalog_all['hostmass'])
        f.create_dataset("weight", data=bh_catalog_all['weight'])
